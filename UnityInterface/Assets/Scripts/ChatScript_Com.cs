using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class ChatScript_Com : MonoBehaviour
{



	public Text botText;
	public GameObject botBubble;
	public InputField playerInputWindow;


	// private string ChatScripUrl = "http://127.0.0.1/sh_client.php?";
	// private string NewIDUrl = "http://127.0.0.1/sh_id.php?";
	private string ChatScripUrl = "http://167.160.163.209/lablablab/sh_client.php?";
	private string NewIDUrl = "http://167.160.163.209/lablablab/sh_id.php?";
	private string userID = "globule";
	private int userNumber = 0;
	private string consoleText = "";
	private int session = 0;
	private bool endgame = false;

	public float textDelay = 0.2f;

	public UndertakerMoodHandler mood;

	public HeadStoneProgress ClaudiusHeadstone;
	public HeadStoneProgress GertrudeHeadstone;
	public HeadStoneProgress PoloniusHeadstone;
	public HeadStoneProgress HamletSrHeadstone;
	public HeadStoneProgress HamletJrHeadstone;
	public HeadStoneProgress OpheliaHeadstone;
	public HeadStoneProgress LaertesHeadstone;
	public HeadStoneProgress RozencrantzHeadstone;
	public HeadStoneProgress GuildensternHeadstone;
    HeadStoneProgress[] hsArray;



    private Lookup<string, HeadStoneProgress> stringToHeadstone;
	public GameObject playerBubble;
	public Text playerText;

	public InputField PlayerInput;

	public GameObject dunnoMainObject;
	public GameObject instantiatedDunno;

	public RectTransform dunnoAnchor;

	public Transform Fireworks;

	IEnumerator Start ()
	{


		hsArray = new HeadStoneProgress[] {
			ClaudiusHeadstone,
			GertrudeHeadstone,
			PoloniusHeadstone,
			HamletSrHeadstone,
			HamletJrHeadstone,
			OpheliaHeadstone,
			LaertesHeadstone,
			RozencrantzHeadstone,
			GuildensternHeadstone
		};

		stringToHeadstone = (Lookup<string,HeadStoneProgress>)hsArray.ToLookup (p => p.identifier, p => p);

		foreach (var item in stringToHeadstone) {
			Debug.Log (item.Key);
		}

		playerBubble.SetActive (false);
		botBubble.SetActive (false);



		yield return StartCoroutine (getNewID ());
		Camera.main.GetComponent<SceneFadeOut> ().StopAllCoroutines ();
		yield return StartCoroutine (Camera.main.GetComponent<SceneFadeOut> ().fadeTo (Color.clear, 1.5f));


		initConversation ();



		playerText = playerBubble.GetComponentInChildren<Text> ();



		//  StartCoroutine(Says(botText, "CDunno"));
		yield return null;

        //yield return StartCoroutine (test ());

	}


    public bool IsGameComplete() {
        foreach (var item in hsArray)
        {



            if (!item.Completed) {
                return false;
            }
        }

        return true;

    }

	IEnumerator test ()
	{
		yield return null;

		string[] characters = new string[] {
			"CClaudius",
			"CGertrude",
			"CPolonius",
			"CHamletSr",
			"CHamletJr",
			"COphelia",
			"CLaertes",
			"CGR"
		};

		string[] milestone = new string[] {
			"When",
			"Who",
			"Where",
			"Why",
			"How"
		};

		foreach (var item in characters) {
			foreach (var item2 in milestone) {
				parseCodes (item + item2);

			}
		}


	}


	void OnGUI ()
	{
		if (Event.current.keyCode == KeyCode.Return && Event.current.type == EventType.keyDown && PlayerInput.text != "") {
			Debug.Log (EventSystem.current.currentSelectedGameObject);
			if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject == PlayerInput.gameObject) {
				sendText (PlayerInput.text);
				PlayerInput.text = "";
			}
		}
	}

	IEnumerator botSays (string bubbletext)
	{



		yield return StartCoroutine (Says (botText, bubbletext));
		//Set focus on input window
		playerInputWindow.interactable = true;
		playerInputWindow.ActivateInputField ();
	}

	public void SetSize (RectTransform trans, Vector2 newSize)
	{
		Vector2 oldSize = trans.rect.size;
		Vector2 deltaSize = newSize - oldSize;
		trans.offsetMin = trans.offsetMin - new Vector2 (deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
		trans.offsetMax = trans.offsetMax + new Vector2 (deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
	}

	void resizeBubble (Text bubble, string bubbletext)
	{
		RectTransform rt = bubble.rectTransform.parent.GetComponent<RectTransform> ();

		rt.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
		bubble.text = bubbletext;
		Canvas.ForceUpdateCanvases ();

		for (int i = 0; i < 10; i++) {
			float width = rt.rect.width;
			float height = rt.rect.height;
			float ratio = width / height;
			Debug.Log (width + "  " + height);
			Debug.Log ("before ratio " + ratio);

			SetSize (rt, new Vector2 (height * 4f, height));
			//rt.sizeDelta = new Vector2(200 ,200);

			Debug.Log (ratio);
			Canvas.ForceUpdateCanvases ();
		}
		rt.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.Unconstrained;
		bubble.text = "";
	}



	IEnumerator Says (Text who, string what)
	{
        
		what = cleanString (what);
		what = parseCodes (what);
		who.text = "";

		if (who == botText) {
			if (instantiatedDunno) {
				Destroy (instantiatedDunno);
			}


			HistoryHandler.AddLine (HistoryName.Bot, what);
			if (what.Length == 0) {
				botBubble.SetActive (false);
				instantiatedDunno = Instantiate (dunnoMainObject) as GameObject;

				var itrans = instantiatedDunno.GetComponent<RectTransform> ();

				itrans.SetParent (this.transform, false);
				itrans.pivot = dunnoAnchor.pivot;
				itrans.anchorMin = dunnoAnchor.anchorMin;
				itrans.anchorMax = dunnoAnchor.anchorMax;
				itrans.sizeDelta = dunnoAnchor.sizeDelta;
				itrans.position = dunnoAnchor.position;
				instantiatedDunno.GetComponent<DunnoScript> ().maxMove = new Vector2 (dunnoAnchor.rect.width, dunnoAnchor.rect.height);

				instantiatedDunno.GetComponent<DunnoScript> ().launch ();

				//hide bubble and show ???
			} else {
				//hide ??? and show bubble
				botBubble.SetActive (true);
			}
		} else {
			HistoryHandler.AddLine (HistoryName.Player, what);
		}

		resizeBubble (who, what);



		foreach (var letter in what.ToCharArray()) {
			who.text += letter;
			yield return new WaitForSeconds (textDelay * Random.Range (0.35f, 0.55f)); // Original Random.Range(0.5, 2)
		}
		/*if (who == botText) {
			playerBubble.SetActive (false);
		}*/
		if(endgame&&(who==botText)){
			StartCoroutine(botSays("Hey! You did it! You solved Hamlet!"));
			Instantiate(Fireworks,new Vector3(0.3f,-17.0f,-60.0f),transform.rotation);
			endgame = false;
		}

	}

	IEnumerator playerSays (string bubbletext)
	{

		playerBubble.SetActive (true);
		StartCoroutine (Says (playerText, char.ToUpper (bubbletext [0]) + bubbletext.Substring (1)));
		yield return null;
	}

	string parseCodes (string parseText)
	{
		//mood
		string[] emo = new string[] { "CBored", "CDunno", "CLol", "CNeutral", "CSuspicious", "CThinking", "CScared" };
		bool moodFound = false;
		foreach (string s in emo) {
			if (parseText.Contains (s)) {
				moodFound = true;
				var newMood = (Mood)System.Enum.Parse (typeof(Mood), s);
				mood.ChangeMood (newMood);
				if (newMood == Mood.CDunno) {
					Debug.Log ("detecteddunno");
					parseText = "";
					return parseText;
				} else {
					parseText = parseText.Remove (parseText.IndexOf (s), s.Length);
				}

			}
		}

		if (!moodFound) {
			mood.ChangeMood (Mood.CNeutral);
		}

		string[] characters = new string[] {
			"CClaudius",
			"CGertrude",
			"CPolonius",
			"CHamletSr",
			"CHamletJr",
			"COphelia",
			"CLaertes",
			"CGR"
		};
		//string[] ms = new string[] { "When","How","Where","Whom","Why"};
		foreach (string cr in characters) {


			if (parseText.Contains (cr)) {

				var startMilestone = parseText.IndexOf (cr) + cr.Length;
				var endMilestone = parseText.IndexOfAny (" ,\n".ToCharArray (), startMilestone);
				endMilestone = endMilestone < 0 ? parseText.Length : endMilestone;
				string milestone = parseText.Substring (startMilestone, endMilestone - startMilestone);


				HeadStoneProgress[] hsp = stringToHeadstone [cr].ToArray ();
				Debug.Log (hsp.Length);
				foreach (var item in hsp) {
					item.activate ((Milestone)System.Enum.Parse (typeof(Milestone), milestone));

				}

				parseText = parseText.Remove (parseText.IndexOf (cr), endMilestone - parseText.IndexOf (cr));

				if(IsGameComplete()){
					endgame = true;
				}
			}
		}
		parseText = System.Text.RegularExpressions.Regex.Replace (parseText, @"\s+", " ");
		return parseText;
	}

	public void sendText (string userInput)
	{

		playerInputWindow.interactable = false;
		playerInputWindow.DeactivateInputField ();

		userInput = cleanString (userInput);
		playSound ();
		consoleText = consoleText + "\n\n[Player]: " + userInput;
		StartCoroutine (playerSays (userInput));
		StartCoroutine (postMessage (userInput, false, false));
	}

	string cleanString (string s)
	{
		s = s.Replace ("\r", "");
		s = s.Replace ("\n", "");
		return s;
	}

	void initConversation ()
	{
		session++;
		userID = "id_" + userNumber + "_session_" + session;
		StartCoroutine (postMessage ("", false, false));
	}

	IEnumerator getNewID ()
	{
		WWW w = new WWW (NewIDUrl);

		yield return w;
		if (!string.IsNullOrEmpty (w.error)) {
			StartCoroutine (botSays ("I have a problem communicating with my brain, please try talking to me later."));
		} else {
			Debug.Log (w.text);
			userNumber = int.Parse (w.text); // Retrieve bot response for display in text bubble
		}
	}

	IEnumerator postMessage (string message, bool pause, bool silent)
	{


		Debug.Log ("sending message");
		message = message.Replace (":", ""); // avoid users sending commands to server
		if (message == "") {
			message = "bob";
		}

		var msgURL = ChatScripUrl + "message=" + WWW.EscapeURL (message) + "&userID=" + WWW.EscapeURL (userID);
		Debug.Log (msgURL);
		WWW w = new WWW (msgURL);

		yield return w;

		if (!string.IsNullOrEmpty (w.error)) {
			StartCoroutine (botSays ("I have a problem communicating with my brain, please try talking to me later."));
		} else {
			if (!silent) {
				StartCoroutine (botSays (w.text.Trim ()));
			}
		}
	}

	// Button sounds
	void playSound ()
	{

	}
}
