﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DunnoScript : MonoBehaviour
{

	// Use this for initialization

	public Vector2 maxMove;
	Vector2 maxScale;

	void Start ()
	{
				

	}

	public void launch ()
	{
		foreach (Text t in GetComponentsInChildren<Text>()) {
			StartCoroutine (move (t));
		}

	}

	IEnumerator move (Text child)
	{
		float i = 0;
		Vector3 start = child.transform.position;
		Vector3 change = new Vector3 (Random.Range (0f, 1f) * maxMove.x, Random.Range (0f, 1f) * maxMove.y, 0);
		//change = change.normalized * 1.5f;


		Vector3 end = new Vector3 (Random.Range (0f, 1f) * maxMove.x, Random.Range (0f, 1f) * maxMove.y, 0);

		Quaternion startRot = child.transform.rotation;
		Quaternion endRot = startRot * Quaternion.Euler (0, 0, Random.Range (-20f, 20f));

		float randomLight = Random.Range (.8f, 1f);
		Color startColor = new Color (randomLight, randomLight, randomLight, 0);
		Color endColor = new Color (randomLight, randomLight, randomLight, 1);

		float startSize = 12;
		float endSize = 40;
		child.color = startColor;
		while (i < 1) {
			i += 0.05f;

			child.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (start, end, i);
			child.transform.rotation = Quaternion.Lerp (startRot, endRot, i);
			child.color = Color.Lerp (startColor, endColor, i);
			child.fontSize = (int)(Mathf.Lerp (startSize, endSize, i));

			yield return null;
		}

	}



	// Update is called once per frame
	void Update ()
	{

	}
}
