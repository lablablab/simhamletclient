using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CaretStick : MonoBehaviour, ISelectHandler
{
    private bool alreadyFixed;
    public void OnSelect(BaseEventData eventData)
    {
        InputField ipFld = gameObject.GetComponent<InputField>();
        if (ipFld != null)
        {
            RectTransform textTransform = (RectTransform)ipFld.textComponent.transform;
            RectTransform caretTransform = (RectTransform)transform.GetChild(0).Find(gameObject.name + " Input Caret");
            if (caretTransform != null && textTransform != null)
            {
							Debug.Log("correct");
                caretTransform.localPosition = textTransform.localPosition;
            }
        }
    }
}
