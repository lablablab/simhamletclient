﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum Milestone
{
	When,
	How,
	Where,
	Who,
	Why
}

public class HeadStoneProgress : MonoBehaviour
{
	Dictionary<Milestone, SpriteRenderer> mToSprite;

	public Sprite onSprite;
	public Sprite offSprite;
	public string When;
	public string How;
	public string Where;
	public string Who;
	public string Why;
	public string pronoun;

	public Color glowColor;

	public string identifier;

	public RectTransform overSprite;
	Text overName;
	Text overText;
	public string CharacterName;

	private ParticleSystem particles;

	bool mouseOver;

	Coroutine glowRoutine;

    private bool completed;

    public bool Completed
    {
        get
        {
            if (completed == true) return true;
            else {
                foreach (var item in mToSprite.Values)
                {
                    if (item.sprite != onSprite) {
                        return false;
                    }
                }
                completed = true;
                return true;
            }
        }
    }



    // Use this for initialization
    void Start ()
	{
		mToSprite = new Dictionary<Milestone, SpriteRenderer> ();

		mToSprite.Add (Milestone.When, transform.Find ("graphics").Find ("when").GetComponent<SpriteRenderer> ());
		mToSprite.Add (Milestone.How, transform.Find ("graphics").Find ("how").GetComponent<SpriteRenderer> ());
		mToSprite.Add (Milestone.Where, transform.Find ("graphics").Find ("where").GetComponent<SpriteRenderer> ());
		mToSprite.Add (Milestone.Who, transform.Find ("graphics").Find ("whom").GetComponent<SpriteRenderer> ());
		mToSprite.Add (Milestone.Why, transform.Find ("graphics").Find ("why").GetComponent<SpriteRenderer> ());

		foreach (SpriteRenderer spr in mToSprite.Values) {
			spr.enabled = true;
			spr.sprite = offSprite;
		}

		overName = overSprite.Find ("name").Find ("text").GetComponent<Text> ();
		overText = overSprite.Find ("description").Find ("text").GetComponent<Text> ();
	}

	void OnMouseExit ()
	{
		if (glowRoutine != null)
			StopCoroutine (glowRoutine);
		glowRoutine = null;

		SpriteRenderer[] sprs = transform.parent.GetComponentsInChildren<SpriteRenderer> ();

		foreach (SpriteRenderer spr in sprs) {
			spr.color = Color.white;
		}
	}

	void OnMouseOver ()
	{

		if (!ModalFade.isOpen && !HistoryHandler.isOpen && glowRoutine == null) {
			glowRoutine = StartCoroutine (glow ());
		}

		if (Input.GetMouseButtonDown (0)) {
			if (!ModalFade.isOpen && !HistoryHandler.isOpen) {
				Open ();
				OnMouseExit ();
			}
		}
	}

	IEnumerator glow ()
	{
		float i = 0;
		float speed = .04f;
		float di = 1;

		SpriteRenderer[] sprs = transform.parent.GetComponentsInChildren<SpriteRenderer> ();

		while (true) {


			foreach (SpriteRenderer spr in sprs) {
				spr.color = Color.Lerp (Color.white, glowColor, i);
			}
			i += speed * di;
			if (i > 1 || i < 0)
				di *= -1;



			yield return null;
		}


	}


	void Open ()
	{
		//overSprite.gameObject.SetActive(true);


		overName.text = CharacterName;
		var textToshow = new List<string> ();

		textToshow.Add (char.ToUpper (pronoun [0]) + pronoun.Substring (1) + " was killed");
		ModalFade.i.On ();
		OverlayToggle.i.On ();
		if (mToSprite [Milestone.When].sprite == onSprite) {
			textToshow.Add (When);
		} else {
			textToshow.Add ("at some moment");
		}
		if (mToSprite [Milestone.How].sprite == onSprite) {
			textToshow.Add (How);
		} else {
			textToshow.Add ("in some way");
		}
		if (mToSprite [Milestone.Where].sprite == onSprite) {
			textToshow.Add (Where);
		} else {
			textToshow.Add ("somewhere");
		}
		if (mToSprite [Milestone.Who].sprite == onSprite) {
			textToshow.Add (Who);
		} else {
			textToshow.Add ("by someone");
		}
		if (mToSprite [Milestone.Why].sprite == onSprite) {
			textToshow.Add (Why);
		} else {
			textToshow.Add ("for some reason");
		}


		textToshow [0] = textToshow [0] + " " + textToshow [1];
		textToshow.RemoveAt (1);
		overText.text = string.Join (", ", textToshow.ToArray ());
		overText.text += ".";


	}




	public void activate (Milestone m)
	{
		mToSprite [m].enabled = true;
		mToSprite [m].sprite = onSprite;

		var pos = mToSprite [m].transform.position;
		Instantiate (Resources.Load ("rockparticles"), pos, Quaternion.Euler (90, 0, 0));

	}

	public void activateAll ()
	{

		foreach (Milestone m in System.Enum.GetValues(typeof(Milestone))) {
			mToSprite [m].enabled = true;
			mToSprite [m].sprite = onSprite;
		}
	}

}
