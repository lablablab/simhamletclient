using UnityEngine;
using System.Collections;

public class TitleDisplay : MonoBehaviour
{
    /*

    This script is used to display and fade titles & logos. Attach it to an empty that you will use as a title object.
    Mostly useful for intros/outros. Could be easily modded to display on bool triggers...

    */
    // Hide the GUIStyle panel but let the user set the parameter that are needed.

    void Awake()
    {
    }

    void Start()
    {
        // Set the GUIStyle parameters

    }

    // Is drawn on every frame & everything here can be adjusted from the inspector.
    void OnGUI()
    {

    }


}
