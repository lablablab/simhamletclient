﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class OverlayToggle : MonoBehaviour
{

    // Use this for initialization
    Material[] mats;

    private static OverlayToggle _instance;

    [Range(-0.1f, 1.1f)]
    public float cutoff = 0;


    public static OverlayToggle i
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<OverlayToggle>();
            }
            return _instance;
        }
    }

    void Start()
    {
        mats = new Material[4];

        mats[0] =  transform.FindChild("name").GetComponent<Image>().material;
        mats[1] = transform.FindChild("description").GetComponent<Image>().material;
        mats[2] = transform.FindChild("name").FindChild("text").GetComponent<Text>().material;
        mats[3] =  transform.FindChild("description").FindChild("text").GetComponent<Text>().material;
        disableAll();
    }


    public void On()
    {
        StopAllCoroutines();
        StartCoroutine(on());

    }

    public void Off()
    {
        StopAllCoroutines();
        StartCoroutine(off());

    }

    private void disableAll(){
      foreach(Image im in gameObject.GetComponentsInChildren<Image>(true))
      {
          Debug.Log("disable image");
          im.enabled = false;
      }

      foreach(Text tx in gameObject.GetComponentsInChildren<Text>(true))
      {
          Debug.Log("disable image");
          tx.enabled = false;
      }
    }

    private void enableAll(){
      foreach(Image im in gameObject.GetComponentsInChildren<Image>(true))
      {
          Debug.Log("disable image");
          im.enabled = true;
      }

      foreach(Text tx in gameObject.GetComponentsInChildren<Text>(true))
      {
          Debug.Log("disable image");
          tx.enabled = true;
      }
    }

    private IEnumerator off()
    {
        cutoff = 0.0f;


        while (cutoff < 1.1f)
        {
            foreach (Material mat in mats)
            {

                mat.SetFloat("_CutOff", cutoff);
            }
            cutoff += 0.05f;

            yield return null;
        }
        disableAll();
    }

    private IEnumerator on()
    {
        //yield return new WaitForSeconds(.2f);
        cutoff = 1.7f;
        enableAll();
        while (cutoff > -0.1f)
        {
            foreach (Material mat in mats)
            {

                mat.SetFloat("_CutOff", cutoff);
            }
            cutoff -= 0.05f;
            yield return null;
        }
    }

    void OnGUI()
    {
        if (Application.isPlaying) return;
        foreach (Material mat in mats)
        {
            mat.SetFloat("_CutOff", cutoff);
        }
    }

}
