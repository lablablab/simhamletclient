using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour
{


    private SpriteRenderer sr;
    public Sprite[] frames;

    void Start()
    {
        sr = transform.root.GetComponent<SpriteRenderer>();

    }

    public void changeState(float curValue, float MaxValue)
    {

        if (curValue < 0) { curValue = 0; }
        if (curValue > MaxValue) { curValue = MaxValue; }
        int newFrame = Mathf.RoundToInt((curValue / MaxValue) * 16);
        //print("frame:"+newFrame);
        sr.sprite = frames[newFrame];


    }
}
