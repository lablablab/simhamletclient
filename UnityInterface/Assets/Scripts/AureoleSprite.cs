using UnityEngine;

public class AureoleSprite : MonoBehaviour
{
    private Component[] aureolesObj; //Array of objects
    private float aurY = 1f; // Y max value for the cos wobble
    private float aurAmp = 0.002f; // Amplitude for the cos wobble
    private float aurIndex; // Index for the cos wobble
    private float aurA; // Alpha target for the fadein
    private float aurCA; // Current alpha for the fade in


    void Awake()
    {
        fillArray();
    }

    void Start()
    {
    }

    void Update()
    {
        aurMovement();
        aurAlpha();
    }

    private void aurMovement()
    { // Update the movement to create a slight wobble
        foreach (Component obj in aureolesObj)
        {
            aurIndex += Time.deltaTime;
            float y = aurAmp * Mathf.Cos(aurY * aurIndex) + Random.Range(0.004f, -0.004f);
            obj.GetComponent<Renderer>().transform.position = new Vector3(obj.GetComponent<Renderer>().transform.position.x, obj.GetComponent<Renderer>().transform.position.y + y, 0); // Make sure that we slide from current y to target y and back
        }
    }

    private void fillArray()
    { // Populate the array
        aureolesObj = gameObject.GetComponentsInChildren(typeof(SpriteRenderer)); // Fetch the gameobjects's children of type "SpriteRenderer"
        foreach (Component obj in aureolesObj)
        {
            Color c = obj.GetComponent<Renderer>().material.color;
            c.a = 0;
            obj.GetComponent<Renderer>().material.color = c; // Make sure that our objects alphas are 0
        }
    }

    private void aurAlpha()
    { // Update our auréoles's alpha
        foreach (Component obj in aureolesObj)
        {
            Color c = obj.GetComponent<Renderer>().material.color;

            aurCA = c.a; // Register current alpha
            c.a += (aurA - aurCA) * 0.1f;
            obj.GetComponent<Renderer>().material.color = c; //Log toward target alpha
        }
    }

    public void aureoleAlphaSet(float alpha, float maxAlpha)
    { // Set the target alpha (use the same value as ProgressBar)
        aurA = 1f * alpha / maxAlpha; // Map value over 1.0 (max alpha float)
    }

}
