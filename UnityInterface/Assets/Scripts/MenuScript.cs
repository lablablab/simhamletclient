using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour
{


    private string consoleText;

    // Text bubble variables
    public float textDelay = 0.2f;
    private string botWords = "Testing, testing, one two, one two";
    private string botCurrentWords = "";
    private bool coupe = false;

    // Flags
    private bool botTalking = false;


    // GUI variables
    public GUIStyle textBubbleStyle;
    public Rect textBubbleCoords;

    public bool centeredWidth = false;
    public bool verticalAlign = false;

    private float groupPosX = 0;

    private int btnLength = Screen.width / 20;
    private int btnHeight = Screen.height / 20;
    private int btnSpacing = Screen.width / 10;

    private bool playBtn;
    private bool tutorialBtn;
    private bool creditsBtn;
    private bool introBtn;

    GUIStyle labelStyle;
    Texture fullScreenIcon;

    public GUIStyle menuStyle;


    // User agent
    private bool chromeBrowser = false;

    //Sounds
    public AudioClip[] selectSfxNumber; //Array to make a random bank of sound variations. Use with AudioSource.PlayClipAtPoint(selectSfxNumber[Random.Range(0, selectSfxNumber.Length)], transform.position);

    void Start()
    {
        menuStyle.fontSize = Screen.width / 50;
        iTween.CameraFadeAdd();
#if UNITY_WEBPLAYER
    testBrowser();
#endif

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && (botCurrentWords.Length > 3))
        {
            botCurrentWords = botWords;
            coupe = true;
        }
    }

    IEnumerator botSays(string bubbletext)
    {
        bubbletext = bubbletext.Replace("\r", "");
        bubbletext = bubbletext.Replace("\n", "");
        while (botTalking == true)
        {
            yield return new WaitForSeconds(0.1f);
        }

        botTalking = true;
        botWords = bubbletext;
        botCurrentWords = "";


        foreach (var letter in bubbletext.ToCharArray())
        {
            if (botCurrentWords == bubbletext) break;
            if (botWords != bubbletext) break;
            botCurrentWords += letter;
            yield return new WaitForSeconds(textDelay * Random.Range(0.25f, 0.5f)); // Original Random.Range(0.5, 2)
        }

        coupe = false;
        for (int i = 0; i < 20; i++)
        {
            if (coupe == true)
            {
                break;
            }
            yield return new WaitForSeconds(0.1f);
        }
        botTalking = false;


    }

    // All buttons sound
    void playSound()
    {
        if (selectSfxNumber.Length >= 1 && selectSfxNumber[0])
        {
            AudioSource.PlayClipAtPoint(selectSfxNumber[Random.Range(0, selectSfxNumber.Length)], transform.position);
        }
    }

    void OnGUI()
    {
        // Bot text bubble
        GUI.skin.label.alignment = TextAnchor.MiddleCenter; // Text alignment
        float textWidth;
        float bubbleHeight;
        float minWidth;
        float maxWidth;

        textWidth = textBubbleCoords.width * Screen.width / 1280;
        bubbleHeight = textBubbleStyle.CalcHeight(new GUIContent(botWords), textWidth);
        textBubbleStyle.CalcMinMaxWidth(new GUIContent(botWords), out minWidth, out maxWidth);
        if (maxWidth > textWidth)
        {
            maxWidth = textWidth;
        }
        if (maxWidth < 100)
            maxWidth = 100;

        // DEFUCK IS THAT FOR ?????
        // GUI.color.a = botCurrentWords.Length;
        // GUI.color.a /= 8;
        // if (botCurrentWords == botWords)
        // 	GUI.color.a = 1;
        GUI.Box(new Rect(textBubbleCoords.x * Screen.width / 1280, textBubbleCoords.y * Screen.height / 800, maxWidth, bubbleHeight), botCurrentWords, textBubbleStyle);
        // GUI.color.a = 1.0;
        //Disable buttons while bot is talking
        GUI.enabled = !botTalking;

        if (centeredWidth)
            groupPosX = Screen.width / 4;
        if (!verticalAlign)
        {
            // All buttons are adjusted to the group. (0,0) is the topleft corner of the group.
            GUI.BeginGroup(new Rect(groupPosX, Screen.height - Screen.height / 20, Screen.width, Screen.height));
            playBtn = GUI.Button(new Rect(btnSpacing, 0, btnLength, btnHeight), "  [PLAY]  ", menuStyle);
            introBtn = GUI.Button(new Rect(btnSpacing + btnLength * 1.75f, 0, btnLength, btnHeight), "  [INTRO]  ", menuStyle);
            tutorialBtn = GUI.Button(new Rect(btnSpacing * 2 + btnLength * 1.75f, 0, btnLength, btnHeight), "[TUTORIAL]", menuStyle);
            creditsBtn = GUI.Button(new Rect(btnSpacing * 3 + btnLength * 1.75f, 0, btnLength, btnHeight), "[CREDITS] ", menuStyle);
            GUI.EndGroup();
        }
        if (verticalAlign)
        {
            GUI.BeginGroup(new Rect(btnLength, btnHeight, Screen.width, Screen.height));
            playBtn = GUI.Button(new Rect(0, 0, btnLength * 2, btnHeight), "[PLAY]", menuStyle);
            tutorialBtn = GUI.Button(new Rect(0, btnHeight, btnLength * 2, btnHeight), "[TUTORIAL]", menuStyle);
            creditsBtn = GUI.Button(new Rect(0, btnHeight * 2, btnLength * 2, btnHeight), "[CREDITS]", menuStyle);
            GUI.EndGroup();
        }


        //Play Button
        if (playBtn)
        {
            playSound();
            botTalking = false;
            iTween.CameraFadeTo(iTween.Hash("time", 1.5f, "alpha", 1, "onComplete", "loadMain", "onCompleteTarget", gameObject));
            // See http://forum.unity3d.com/threads/itween-camerafade.64104/#post-424538
        }

        //Tutorial Button
        if (introBtn)
        {
            playSound();
            botSays("SimProphet is a very ulmash game in which you are the prophet.");
            botSays("A 'choose your own religion game' if you prefer.");
            botSays("The context is you've just met a divinity and are on a mission to spread a new faith.");
            botSays("The first person you stumble upon is Ambar, a humble Babylonian shepherd.");
            botSays("It is your duty to inform him of your god's message in the hope of making him a follower.");
            botSays("Press the [play] button to get started or [tutorial] for more technical information");
        }

        //Tutorial Button
        if (tutorialBtn)
        {
            playSound();
            botSays("Talking to Ambar is simple...");
            botSays("Just talk to him in plain English!");
            botSays("He might not understand everything, though.");
            botSays("But don't let that discourage you.");
            botSays("The progress bars at the bottom of the screen will inform you of whatever progress you're making.");
            botSays("At some point Ambar will either join your cult willfully or grow tired of the discussion.");
        }

        //Credits Button
        if (creditsBtn)
        {
            playSound();
            botSays("SimProphet was developed at the lablablab (lablablab.net).");
            botSays("Lead design, writing, scripting and code by Jonathan Lessard.");
            botSays("Additionnal design, writing, scripting and code by Samuel Cousin.");
            botSays("Illustrations by Olivia Colden.");
            botSays("Chatscript engine by Bruce Wilcox.");
        }
        //Fullscreen btn
        /*if (GUI.Button(new Rect (1240* Screen.width / 1280,720* Screen.height / 800,32 * Screen.width / 1280,32* Screen.height / 800), fullScreenIcon, labelStyle))
        {
        Screen.fullScreen = !Screen.fullScreen;
        }*/

        /*if (chromeBrowser) {
            playSound();
            botSays("Seems like you're using Chrome.");
            botSays("You might have to refresh your browser window to speak with Ambar.");
            chromeBrowser = !chromeBrowser;
        }*/

        if (Event.current.isKey || Event.current.isMouse)
        {
            if (!botTalking && botCurrentWords == botWords)
            {
                botCurrentWords = "";
            }
        }

    }

    void loadMain()
    {
        Application.LoadLevel("Main");
    }

    // Fire the isBrowserChrome() function in the webpage
    void testBrowser()
    {
        Application.ExternalEval("isBrowserChrome()");
    }

    // Is called back only if isBrowserChrome() is true
    void browserIsChrome(string param)
    {
        chromeBrowser = !chromeBrowser;
    }

}
