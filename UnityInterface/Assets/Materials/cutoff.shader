﻿Shader "Custom/cutoff" {
			Properties {
	        [PerRendererData] _MainTex ("Base (RGB)", 2D) = "white" {}
					_Color ("Tint", Color) = (1,1,1,1)
					_CutOff("Cut off",Range(0,1)) = 1
					_CutoffTexture("Cut Off Texture", 2D ) = "white" {}
					[MaterialToggle] _text("is text", Float) = 0
	    }
	    SubShader {
					Tags { "Queue"="Transparent" "RenderType"="Transparent" }
					ZWrite Off
					Blend SrcAlpha OneMinusSrcAlpha
	        Pass {

	            CGPROGRAM
	            #pragma vertex vert
	            #pragma fragment frag

	            #include "UnityCG.cginc"

	            uniform sampler2D _MainTex;
							uniform sampler2D _CutoffTexture;
							uniform fixed4 _Color;
							uniform float _CutOff;
							uniform float _text;

							struct appdata_t
							{
								float4 vertex   : POSITION;
								float4 color    : COLOR;
								float2 uv : TEXCOORD0;
							};

							struct v2f
							{
								float4 vertex   : SV_POSITION;
								fixed4 color    : COLOR;
								half2 uv  : TEXCOORD0;
								fixed3 screenPos : POSITION2;
							};

							v2f vert(appdata_t IN)
							{
								v2f OUT;
								OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
								OUT.uv = IN.uv;
								OUT.color = IN.color * _Color;
								OUT.screenPos = ComputeScreenPos(OUT.vertex);
								return OUT;
							}

	            fixed4 frag(v2f i) : SV_Target {
									if(_text ==0)
									{
										clip(tex2D(_CutoffTexture, i.screenPos).r-_CutOff);
										float4 ret = tex2D(_MainTex, i.uv)*i.color;
										return ret;
									}else{
										clip(tex2D(_CutoffTexture, i.screenPos).r-_CutOff);


										fixed4 col = i.color;
										col.a *= tex2D(_MainTex, i.uv).a;
										clip (col.a - 0.01);
										return col;
									}




	              //  return tex2D(_MainTex, i.uv)*i.color;
	            }
	            ENDCG
	        }
	    }
}
